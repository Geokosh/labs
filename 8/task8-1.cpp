#include "Automata.h"
#include <iostream>

using namespace std;

void main()
{
	char str = 'g';
	cout << "Please, enter '1' to enable coffee mashine or '0' to exit from the programm." << endl;
	while (str != '1')
	{
		cin >> str;
		switch (str)
		{
		case '1': break;
		case '0': exit(1); break;
		default: cout << "Input error!!! Try again..." << endl;
		}
	}//while

	Automata cof;
	cof.on();

	cout << "Hello!! Today we have:" << endl << endl;
	cof.printMenu();

	int ch;
	cout << endl << "Please, choose a drink. Enter of number: ";
	cin >> ch;
	cout << endl;
	cof.choice(ch);

	int money;
	cout << "Please, input money: ";
	cin >> money;
	cout << endl;
	cof.coin(money);

	while (cof.check != true)
	{
		cout << "Please, input correct sum: ";
		cin >> money;
		cof.coin(money);
	}

	cof.cook();
	cof.finish();

	cout << "If you want to turn off the coffee mashine input '0'." << endl;
	cin >> str;
	if (str == 0)
	{
		cof.off;
		cout << "If you want to exit from the programm input '1'." << endl;
		cin >> str;
		if (str == 1)
			exit(1);
	}
}