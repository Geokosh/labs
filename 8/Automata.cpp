#include "Automata.h"
#include <iostream>

using namespace std;


Automata::Automata(void)
{
	cash = 0;
	char buf[51];
	int count = 10, i = 0;
	int flag = 1;

	numberofdrinks = 0;
	state = OFF;

	FILE *fp = fopen("menu.txt", "r");
	if (fp == NULL)
	{
		perror("");
		exit(1);
	}

	while (fgets(buf, 51, fp))
	{
		numberofdrinks++;
	}

	numberofdrinks /= 2;
	rewind(fp);
	menu = new MENU[numberofdrinks];

	for (int i = 0; i < numberofdrinks; i++)
	{
		fgets(buf, 51, fp);
		buf[strlen(buf) - 1] = 0;
		strcpy(menu[i].drink, buf);
		fgets(buf, 51, fp);
		menu[i].price = atoi(buf);
	}
}

void Automata::on()
{
	if (state != OFF)
		cout << "Not avaiable";
	else
	{
		state = WAIT;
		printState();
	}
}

void Automata::off()
{
	if (state != WAIT)
		cout << "Not avaiable";
	else
	{
		state = OFF;
		printState();
	}
}

void Automata::coin(int value)
{
	cash += value;
	state = ACCEPT;
	printState();
	cout << "Money: " << cash << endl;
}

void Automata::printMenu()
{
	for (int i = 0; i < numberofdrinks; i++)
		cout << i << ' ' << menu[i].drink << '\t' << menu[i].price << endl;
}

void Automata::printState()
{
	switch (state)
	{
	case 0: cout << "status: OFF" << endl; break;
	case 1: cout << "status: WAIT" << endl; break;
	case 2: cout << "status: ACCEPT" << endl; break;
	case 3: cout << "status: CHECK" << endl; break;
	case 4: cout << "status: COOK" << endl; break;
	}
}

void Automata::choice(int ch)
{
	choose = ch;
}

bool Automata::check()
{
	cout << endl;
	state = CHECK;
	printState();

	if (cash < menu[choose].price)
	{
		cout << "Not enough money" << endl << endl;
		cancel();
		return false;
	}
	else if (cash == menu[choose].price)
	{
		cout << "Everything fine!" << endl;
		return true;
	}
	else if (cash > menu[choose].price)
	{
		cout << "Here's your change: " << cash - menu[choose].price << endl;
		cash = 0;
		return true;
	}
}

void Automata::cancel()
{
	if ((state == ACCEPT) | (state == CHECK))
	{
		state = WAIT;
		cout << "return the money (" << cash << ") to you" << endl;
		cash = 0;
	}
	else cout << "Not avaiable";
}

void Automata::cook()
{
	state = COOK;
	printState();
	cout << "Wait until your drink will be ready" << endl;
}

void Automata::finish()
{
	cout << "Enjoy your drink! Come again!" << endl;
	state = WAIT;
	printState();
}