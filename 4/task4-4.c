#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/*
* ���������, ������������� ��������� �����. ������ �������� �� ����� ������ ������.
* ������� �� ����� ������ � ��������� �������.
*/

char **enterLines(int *count);

int main()
{
	char **arr;
	int count = 0, i;

	arr = enterLines(&count);

	int *num = (int*)calloc(count, sizeof(int));

	//���� ��� �������� ������� �� ��������������� �����
	for (i = 0; i < count; i++)
	{
		loop1:
			srand(time(0));
			num[i] = rand() % count;
			for (int j = 0; j < i; j++)
				if (num[j] == num[i])
					goto loop1;
	}

	for (i = 0; i < count; i++)
		printf("%s", arr[num[i]]);

	for (i = 0; i < count; i++)
		free(arr[i]);
	free(arr);
	free(num);

	return 0;
}