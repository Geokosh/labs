#include <stdio.h>
#include <time.h>

/*
* ���������, ������������� ��������� �����. ������ �������� �� ����� ������ ������.
* ������� �� ����� ������ �� ��������� �������.
*/

char **enterLines(int *count);

int main()
{
	char **arr;
	int count = 0, i;

	arr = enterLines(&count);

	srand(time(0));
	i = rand() % count;

	printf("String No%d\n", i);
	printf("%s", arr[i]);

	putchar('\n');

	for (int i = 0; i < count; i++)
		free(arr[i]);
	free(arr);

	return 0;
}