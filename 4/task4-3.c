#include <stdio.h>
#include <stdlib.h>

/*
* ���������, ������������� ��������� �����. ������ �������� �� ����� ������ ������.
* ������� ������ �� ����� � ������� ����������� �� ����.
*/

#define SWAP(A, B) { int tmp = A; A = B; B = tmp; }

char **enterLines(int *count);

int main()
{
	char **arr;
	int count = 0;

	arr = enterLines(&count);

	//��������� ������ �� �� �����
	for (int i = count - 1; i >= 0; i--)
		for (int j = 0; j < i; j++)
			if (strlen(arr[j]) > strlen(arr[j + 1]))
				SWAP(arr[j], arr[j + 1]);

	for (int i = 0; i < count; i++)
		printf("%s", arr[i]);

	for (int i = 0; i < count; i++)
		free(arr[i]);
	free(arr);

	return 0;
}