#include <stdio.h>

/*
* программа, запрашивающая несколько строк. Строки вводятся до ввода пустой строки.
* Выводит строки на экран в обратном порядке.
*/

char **enterLines(int *count);

int main()
{
	char **arr;
	int count = 0;

	arr = enterLines(&count);

	for (int i = count - 1; i >= 0; i--)
		printf("%s", arr[i]);

	putchar('\n');

	for (int i = 0; i < count; i++)
		free(arr[i]);
	free(arr);

	return 0;
}