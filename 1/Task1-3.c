#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

/*
* программа, подсчитывающая сумму всех чисел во введенной строке
*/

int main() {
	int sum = 0;
	int temp = 0;
	char str;

	printf("Hello!! Enter string: ");
	scanf("%c", &str);

	while (str>30) {
		if ((str >= '0') && (str <= '9'))	
			temp = temp * 10 + str - '0';
		else {
			sum += temp;
			temp = 0;
		}//else
		scanf("%c", &str);
	}//while

	sum += temp;
	printf("Sum = %d\n\n", sum);

	return 0;
}//main