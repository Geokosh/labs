#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

/*
* Имеется строка, содержащая символьное представление числа, например "12345"
* Программа преобразовывает в строку, где группы разрядов отделены пробелами, то есть "12 345"
*/

int main() {
	int i = 0, j, count = 0;
	long int num;
	char buf, str[256] = { 0 };

	printf("Hello!! Enter your string: ");
	scanf("%d", &num);

	while (num != 0) {
		str[i] = num % 10 + '0';
		count++;
		i++;
		if (count % 3 == 0 && num > 9) {
			str[i] = ' '; 
			i++;
		}//if
		num = num / 10;
	}//while

	for (i = 0, j = strlen(str) - 1; i < j; i++, j--) {
		buf = str[i];
		str[i] = str[j];
		str[j] = buf;
	}//for
	
	puts(str);

	return 0;
}