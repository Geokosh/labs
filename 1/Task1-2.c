﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

/*
* программа, принимающую величину роста в футы'дюймы и выводящую в см 
* В одном футе 12 дюймов, в 1 дюйме 2.54 см
* При неправильном вводе программа выводит сообщение об ошибке
*/

int main() {
	int ft = -1;
	int inch = -1;
	char c = '0';
	float sm;

	printf("Hello!! Enrer your growth in ft: ");
	scanf("%d%c%d",&ft,&c,&inch);
	sm = (inch + ft * 12) * 2.54f;

	//39 - код апострофа
	while ((c != 39) || (ft == -1) || (inch == -1)) {
		puts("Error! Enter again:");
		fflush(stdin);
		scanf("%d%c%d", &ft, &c, &inch);			
		sm = (inch + ft * 12) * 2.54f;
	}//while
	
	printf("Your growth in sm: %.2f\n", sm);

	return 0;
}