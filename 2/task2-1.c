
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <conio.h>

int main () {
	char *arr;
	int len, i, num;
	printf ("Enter length: ");
	scanf ("%d", &len);
	arr = (char *) malloc (len * sizeof (char));
	for (i = 0; i < len; i++) {
		num = 1 + rand () % 3;
		switch (num) {
		case 1:
			arr[i] = 'A' + rand() % 26;
			break;
		case 2:
			arr[i] = 'a' + rand() % 26;
			break;
		case 3:
			arr[i] = '0' + rand() % 10;
			break;
		default:
			printf ("Error");
		}
		printf ("%c", arr[i]);
	}
	free (arr);
	_getch ();
	return 0;
}