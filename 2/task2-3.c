
#define N 256

#include <stdio.h>
#include <conio.h>
#include <string.h>

int main () {
	int arr[N];
	int i;
	char str[N];
	for (i = 0; i < N; i++)
		arr[i] = 0;
	printf ("Enter string, please (max 256 symbols: ");
	scanf ("%[^\n]", str);
	for (i = 0; i < strlen (str); i++)
		arr[(int)str[i]]++;
	for (i = 0; i < N; i++)
		if (arr[i] != 0)
			printf ("%c - %d\n", (char)i, arr[i]);
	_getch ();
	return 0;
}