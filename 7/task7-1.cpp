#include <stdio.h>
#include "circle.h"

#define RAD 6378100 //������ �����

void dialog()
{
	puts("Program: \"The Earth and a rope\".");
	putchar('\n'); putchar('\n');
	puts("Description of functions:");
	putchar('\n');
	puts("\"SetRad\": enters the radius and considers circle ference and area;");
	puts("\"GetRad\": return radius of circle");
	puts("\"SetFer\": enters the ference and considers circle radius and area;");
	puts("\"GetFer\": return ference of circle");
	puts("\"SetArea\": enters the area and considers circle ference and radius;");
	puts("\"GetArea\": return area of circle");
	putchar('\n'); putchar('\n');
	puts("Constants:");
	putchar('\n');
	puts("PI = 3.14159265358979323846");
	puts("Radius of Earth = 6378100 meters");
	putchar('\n'); putchar('\n');
}

void main()
{
	dialog();

	circle cir;
	double distance, len1;

	cir.SetRad(RAD); //�������� ����� ���������� � ������� �����
	len1 = cir.GetFer();
	cir.SetFer(len1 + 1); //��������� � ������� ��� 1 ����, �������� ����� ������ � �������
	distance = cir.GetRad() - RAD; //��������� = ����� ������ - ������ �����

	printf("Distance between the rope and the Earth: %f meters\n\n", distance);
}