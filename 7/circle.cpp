#include "circle.h"
#include <math.h>

#define PI 3.14159265358979323846

circle::circle()
{
	Radius = Ference = Area = 0;
}

void circle::SetRad(double rad)
{
	Ference = rad * 2 * PI;
	Area = rad * rad * PI;
}

void circle::SetFer(double fer)
{
	Radius = fer / (2 * PI);
	Area = Radius * Radius * PI;
}

void circle::SetArea(double area)
{
	double tmp = area / PI;
	Radius = sqrt(tmp);
	Ference = Radius * 2 * PI;
}