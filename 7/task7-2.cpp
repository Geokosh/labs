#include <stdio.h>
#include "circle.h"

#define RADPOOL 3 //������ ��������
#define WIDTH 1 //������ �������
#define CEMENT 1000 //���� �� 1 ��.� ������
#define FENCE 2000 //���� �� 1 ���� ������

void dialog()
{
	puts("Program: \"The Earth and a rope\".");
	putchar('\n'); putchar('\n');
	puts("Description of functions:");
	putchar('\n');
	puts("\"SetRad\": enters the radius and considers circle ference and area;");
	puts("\"GetRad\": return radius of circle");
	puts("\"SetFer\": enters the ference and considers circle radius and area;");
	puts("\"GetFer\": return ference of circle");
	puts("\"SetArea\": enters the area and considers circle ference and radius;");
	puts("\"GetArea\": return area of circle.");
	putchar('\n'); putchar('\n');
	puts("Constants:");
	putchar('\n');
	puts("PI = 3.14159265358979323846;");
	puts("Radius of pool = 3 meters;");
	puts("Width of footpath = 1 meters;");
	puts("Price of cement = 1000 rub;");
	puts("Price of fence = 2000 rub;");
	putchar('\n'); putchar('\n');
}

void main()
{
	dialog();

	circle cir;
	double prFoot, prFence, s1, s2;

	cir.SetRad(RADPOOL); //�������� ������� ��������
	s1 = cir.GetArea();
	cir.SetRad(WIDTH + RADPOOL); //�������� ������� � ����� �������� �����
	s2 = cir.GetArea();
	prFoot = (s2 - s1) * CEMENT;

	prFence = cir.GetFer() * FENCE;

	printf("The cost of footpath: %.2f\n", prFoot);
	printf("The cost of fence: %.2f\n", prFence);
	printf("Sum: %.2f\n\n", prFoot + prFence);
}